import React, { useState, useEffect } from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import Head from 'next/head';
// import LineChart from '../../components/LineChart';
import moment from 'moment';
import { Line } from 'react-chartjs-2';
import Link from 'next/link';
import {Button} from 'react-bootstrap';
import AppHelper from '../../app-helper';


export default function Record() {

    const [records, setRecords] = useState([]);

    const [dateStart, setDateStart] = useState(new Date());
    const [dateEnd, setDateEnd] = useState(new Date());

    const [trendDates, setTrendDates] = useState([]);
    const [balanceRecords, setBalanceRecords] = useState([]);

    // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            // console.log(data.records)

            setRecords(data.records) 

        })

    }, [])

    useEffect(() => {

        let datesArr = [];
        let amount = 0;
        let balance = [];

        records.forEach(record => {
            if((moment(record.createdOn).format('YYYY-MM-DD') >= dateStart) && (moment(record.createdOn).format('YYYY-MM-DD') <= dateEnd)){
                if(datesArr.includes(moment(record.createdOn).format('YYYY-MM-DD')) !== true){
                    if(record.type === 'Income'){
                        amount=amount + record.amount
                        balance.push(amount)
                    }
                    if(record.type === 'Expense'){
                        amount=amount - record.amount
                        balance.push(amount)
                    }

                    datesArr.push(moment(record.createdOn).format('YYYY-MM-DD'))
                    
                } else {
                    if(record.type === 'Income'){
                        amount = balance[balance.length -1] + record.amount
                        balance.pop();
                        balance.push(amount)
                    }
                    if(record.type === 'Expense'){
                        amount = balance[balance.length - 1] - record.amount
                        balance.pop();
                        balance.push(amount)
                    }
                }
            }

            setTrendDates(datesArr);
            setBalanceRecords(balance);

            // console.log(datesArr)
            // console.log(balance)
        })


    }, [dateStart, dateEnd])

    const data = {
        labels: trendDates,
        datasets: [{
            label: 'Balance',
            fill: true,
            borderColor: 'rgba(255, 99, 132, 1)',
            tension: 0.1,
            data: balanceRecords
        }]
    }

    
    return (
        <React.Fragment>
            <Head>
                <title>Balance Trend</title>
            </Head>
            <h2 className="py-4">Balance Trend</h2>
            <Row>
                <Col>
                    <Form>
                        <Form.Group>
                            <Form.Label>From</Form.Label>
                            <Form.Control 
                                type="date"
                                value={dateStart}
                                onChange={e => setDateStart(e.target.value)}
                            />
                        </Form.Group>
                    </Form>
                </Col>
                <Col>
                    <Form>
                        <Form.Group>
                            <Form.Label>To</Form.Label>
                            <Form.Control 
                                type="date"
                                value={dateEnd}
                                onChange={e => setDateEnd(e.target.value)}
                            />
                        </Form.Group>               
                    </Form>
                </Col>
            </Row>
            <Line data={data}/>
            
            <Link href="/records/index">
                  <Button className="mb-4" variant="primary">
                  <a className="fa fa-plus" aria-hidden="true" role="button">
                    Back to Records
                  </a>
                </Button>
                </Link>

        </React.Fragment>
        
    )
}

