import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function index() {
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {
        e.preventDefault();

        fetch('http://localhost:4000/api/users/email-exists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === false){
                fetch('http://localhost:4000/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        setFirstName('');
                        setLastName('');
                        setMobileNo('');
                        setEmail('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                          icon: 'success',
                          title: 'Thank you for registering!',
                          showConfirmButton: false,
                          timer: 1500
                        })

                        Router.push('/login');
                    }
                })
            }
        })

        // console.log('Thank you for registering!');
    }

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
            },[email, password1, password2]);

        return (
            <React.Fragment>
                <h4 className="text-center pt-5 mt-5">Let's start by creating your</h4>
                <h4 className="text-center pb-3">secure account.</h4>
                <Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-3">
                    <Form.Group>
                        <Form.Control 
                            type="firstName"
                            placeholder="First Name"
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Control 
                            type="lastName"
                            placeholder="Last Name"
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Control 
                            type="text"
                            placeholder="Mobile No."
                            value={mobileNo}
                            onChange={e => setMobileNo(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Control 
                            type="email"
                            placeholder="Email Address"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Control 
                            type="password"
                            placeholder="Password"
                            value={password1}
                            onChange={e => setPassword1(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Control 
                            type="password"
                            placeholder="Verify Password"
                            value={password2}
                            onChange={e => setPassword2(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group className="text-center">
                        {isActive
                            ?
                            <Button 
                                className=" rounded-pill"
                                variant="primary"
                                type="submit"
                                id="submitBtn"
                                block
                            >
                                Register
                            </Button>
                            :
                            <Button 
                                className=" rounded-pill"
                                variant="primary"
                                type="submit"
                                id="submitBtn"
                                disabled
                                block
                            >
                                Register
                            </Button>
                        }
                    </Form.Group>

                    <h6 className="text-center py-2">Already have an account?<a href="/login"> Sign in</a></h6>
                                
                </Form>
            </React.Fragment>
        )
};

