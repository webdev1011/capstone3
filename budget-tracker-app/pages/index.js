import Head from 'next/head';
import Link from 'next/link';
import { Button } from 'react-bootstrap';


export default function Home() {

	return (
		<React.Fragment>
			<div className="text-center my-5">
				<h1 className="text-center mt-5 pt-5">Monitor your spending</h1>
				<p>Try my Budget Tracket App.</p>
				<Link href="/register">
	                <Button className="mb-2" variant="success">
	                    Sign up!
	                </Button>                
	            </Link>
			</div>
		</React.Fragment>
	)
  
}