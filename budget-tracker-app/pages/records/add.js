import { useState, useEffect } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';
import View from '../../components/View';
import AppHelper from '../../app-helper';


export default function AddRecord() {

    return ( 
        <View title={ 'Add New Record' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Record Information</h3>
                    <AddForm />
                </Col>
            </Row>
        </View>
    )
}


const AddForm = () => {

    const [type, setType] = useState('');
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [amount, setAmount] = useState(0);

    const [categories, setCategories] = useState('')

    // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
    useEffect(() => {
        // console.log(localStorage.getItem('token'))
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            console.log(data.categories)

            console.log(type)
        })
})
    function addRecord(e) {
        e.preventDefault();

        fetch('http://localhost:4000/api/users/add-record', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ AppHelper.getAccessToken() }`
            },
            body: JSON.stringify({
                name: name,
                type: type,
                categories: categories,
                description: description,
                amount: amount
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                setName('');
                setType('');
                setCategories('')
                setDescription('');
                setAmount(0);

                Swal.fire({
                      icon: 'success',
                      title: 'New record successfully added',
                      showConfirmButton: false,
                      timer: 1500
                    })

                Router.push('/records')
            }

        })
    }
    return(

        <Card>
            <Card.Header>Add Record</Card.Header>
            <Card.Body>
                <Form onSubmit={e => addRecord(e)}>
                    
                    <Form.Group>
                        <Form.Label>Category Type</Form.Label>
                        <Form.Control 
                            as="select" 
                            value={ type } 
                            onChange={ (e) => setType(e.target.value) }  
                            required>
                                <option value="" disabled selected>Select Category</option>
                                <option>Income</option>
                                <option>Expense</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Category Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            value={ name } 
                            onChange={(e) => setName(e.target.value)} 
                            autoComplete="off" 
                            required
                        />
                    </Form.Group>
                    <Form.Group controlId="amount">
                        <Form.Label>Amount</Form.Label>
                        <Form.Control 
                            type="number" 
                            value={ amount } 
                            onChange={(e) => setAmount(e.target.value)} 
                            autoComplete="off" 
                            required
                        />
                    </Form.Group>
                    <Form.Group controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                            type="text" 
                            value={ description } 
                            onChange={(e) => setDescription(e.target.value)} 
                            autoComplete="off" 
                            required
                        />
                    </Form.Group>
                    
                    <Button 
                        className="mb-2"
                        variant="info"
                        type="submit" 
                        class="btn btn-primary btn-sm"
                    >
                        Submit
                    </Button>
                    
                </Form>
            </Card.Body>
        </Card>

    )
}