import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import IncomeBarChart from '../../components/IncomeBarChart';
import AppHelper from '../../app-helper';
import Link from 'next/link';
import {Button} from 'react-bootstrap';


export default function Record() {

    const [records, setRecords] = useState([]);
    const [incomeRecords, setIncomeRecords] = useState([]);

    // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            // console.log(data.records)

            setRecords(data.records) 

        })

    }, [])

    useEffect(() => {

        let income = []

        records.forEach(element => {

            if(element.type === "Income"){
                income.push(element)
            }

        })

        // console.log(income)

        setIncomeRecords(income)

    }, [records])


    return (
        <React.Fragment>
            <Head>
                <title>Monthly Income</title>
            </Head>
            <h2 className="py-4">Monthly Income in PHP</h2>
            <IncomeBarChart incomeRecords={incomeRecords}/>

                <Link href="/records/index">
                  <Button className="mb-4" variant="primary">
                  <a className="fa fa-plus" aria-hidden="true" role="button">
                    Back to Records
                  </a>
                </Button>
                </Link>

        </React.Fragment>
        
    )
}

